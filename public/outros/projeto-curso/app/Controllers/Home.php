<?php namespace App\Controllers;

use App\Models\ChamadosModel;

class Home extends BaseController
{
	public function index()
	{
		return view('home/index');
	}

	public function cadastroAcao()
	{

		helper('form');

		$validation = \Config\Services::validation();
		
		$validation->setRules([
			'txtTitulo' => ['label' => 'Título', 'rules' => 'required|min_length[8]'],
			'txtDescricao' => ['label' => 'Descrição', 'rules' => 'required']
		]);

		if( ! $validation->withRequest($this->request)->run()){

			return $this->index();

		}else{

			$criado = date('Y-m-d H:i:s');
			$idUsuario = $this->session->get('id');

			$data = [
				'id_usuario' => $idUsuario,
				'titulo' => $this->request->getPost('txtTitulo'),
				'descricao' => $this->request->getPost('txtDescricao'),
				'criado' => $criado,
				'status' => 'Pendente'
			];

			$ChamadosModel = new ChamadosModel();

			if($ChamadosModel->save($data)){

				$idChamado = $ChamadosModel->insertID();

				unset($data);

				$data = [
					'id_chamado' => $idChamado,
					'id_usuario' => $idUsuario,
					'resposta' => '---',
					'status' => 'Pendente',
					'atualizado' => $criado
				];

				$builder = $this->db->table('chamados_historico');
				$builder->insert($data);

				$this->session->setFlashdata('sucesso', 'Chamado cadastrado com sucesso. Código do chamado: #' . $idChamado);

			}else{
				$this->session->setFlashdata('erro', 'Não foi possível realizar o cadastro.');
			}

			return redirect()->to('/home');

		}
		
	}

	//--------------------------------------------------------------------

}
