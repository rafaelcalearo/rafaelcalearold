<?php namespace App\Controllers;

use App\Models\ChamadosModel;

class Chamados extends BaseController
{
	public function index()
	{
		$id = $this->session->get('id');
		$sql = "SELECT * FROM `chamados` ";
		$where = '';

		//FILTRA TITULO E STATUS
		if(! empty($_GET['txtTitulo']) && ! empty($_GET['txtStatus'])){

			$where .= " WHERE `titulo` LIKE '%". $this->db->escapeLikeString($_GET['txtTitulo']) . "%' ESCAPE '!' ";
			$where .= " AND `status` LIKE '%" . $this->db->escapeLikeString($_GET['txtStatus']) ."%' ESCAPE '!'";

		}
		//FILTRA SOMENTE TITULO
		else if(! empty($_GET['txtTitulo'])){

			$where .= " WHERE `titulo` LIKE '%". $this->db->escapeLikeString($_GET['txtTitulo']) . "%' ESCAPE '!' ";

		}
		//FILTRA SOMENTE STATUS
		else if(! empty($_GET['txtStatus'])){

			$where .= " WHERE `status` LIKE '%". $this->db->escapeLikeString($_GET['txtStatus']) . "%' ESCAPE '!' ";

		}

		//VALIDAÇÃO PRA RESGATAR SOMENTE OS CHAMADOS DO USUARIO
		if($this->session->get('nivel') == 'Usuario'){
			$where = ($where == '' ? " WHERE `id_usuario` = {$id}" : $where . " AND `id_usuario` = {$id}");
		}

		$sql .= $where . " ORDER BY `status` ASC, `criado` DESC ";

		$query = $this->db->query($sql);

		$data = [
			'titulo' => 'Listagem de Chamados',
			'chamados' => $query->getResult()
		];

		return view('chamados/index', $data);
    }
    
    public function responder($id)
	{
		
		$ChamadosModel = new ChamadosModel();
		$idUsuario = $this->session->get('id');
		$nivelUsuario = $this->session->get('nivel');

		if($nivelUsuario == 'Usuario'){
			$chamado = $ChamadosModel->select('chamados.id, chamados.titulo, chamados.descricao, chamados.criado, chamados.`status`, usuarios.nome')
									 ->join('usuarios', ' usuarios.id = chamados.id_usuario')
									 ->where('chamados.id', $id)
									 ->where('chamados.id_usuario', $idUsuario)
									 ->find();
		}else{
			$chamado = $ChamadosModel->select('chamados.id, chamados.titulo, chamados.descricao, chamados.criado, chamados.`status`, usuarios.nome')
									 ->join('usuarios', ' usuarios.id = chamados.id_usuario')
									 ->where('chamados.id', $id)
									 ->find();
		}

		if(count($chamado) == 0){
			$this->session->setFlashdata('erro', 'Acesso de forma inadequada.');
			return redirect()->to('/chamados');
		}

		$historico = $ChamadosModel->select('chamados_historico.`status`, chamados_historico.atualizado, chamados_historico.resposta, usuarios.nome')
									->join('chamados_historico', 'chamados_historico.id_chamado = chamados.id')
									->join('usuarios', 'usuarios.id = chamados_historico.id_usuario')
									->where('chamados.id', $id)
									->orderBy('chamados_historico.atualizado', 'DESC')
									->findAll();

		$data = [
			'titulo' => 'Responder Chamado',
			'chamado' => $chamado,
			'historico' => $historico
		];

		return view('chamados/responder', $data);
	}

	public function responderAcao($id)
	{

		helper('form');

		$validation = \Config\Services::validation();
		
		$validation->setRules([
			'txtResposta' => ['label' => 'Resposta', 'rules' => 'required'],
			'txtStatus' => ['label' => 'Status', 'rules' => 'required']
		]);

		if( ! $validation->withRequest($this->request)->run()){

			return $this->responder($id);

		}else{

			$data = [
				'id_chamado' => $id,
				'id_usuario' => $this->session->get('id'),
				'resposta' => $this->request->getPost('txtResposta'),
				'status' => $this->request->getPost('txtStatus'),
				'atualizado' => date('Y-m-d H:i:s')
			];

			$builder = $this->db->table('chamados_historico');

			if($builder->insert($data)){

				$ChamadosModel = new ChamadosModel();

				$ChamadosModel->save([
					'id' => $id,
					'status' => $this->request->getPost('txtStatus')
				]);

				$this->session->setFlashdata('sucesso', 'Resposta cadastrado com sucesso.');
			}else{
				$this->session->setFlashdata('erro', 'Não foi possível cadastrar sua resposta agora.');
			}

			return redirect()->to('/chamados/responder/' . $id);

		}

	}

	public function excluir($id)
	{
		$ChamadosModel = new ChamadosModel();

		$idUsuario = $this->session->get('id');
		$nivelUsuario = $this->session->get('nivel');

		if($nivelUsuario == 'Usuario'){
			$chamado = $ChamadosModel->select('chamados.id')
									 ->join('usuarios', ' usuarios.id = chamados.id_usuario')
									 ->where('chamados.id', $id)
									 ->where('chamados.id_usuario', $idUsuario)
									 ->find();
		}else{
			$chamado = $ChamadosModel->select('chamados.id')
									 ->join('usuarios', ' usuarios.id = chamados.id_usuario')
									 ->where('chamados.id', $id)
									 ->find();
		}

		if(count($chamado) == 0){
			$this->session->setFlashdata('erro', 'Acesso de forma inadequada.');
			return redirect()->to('/chamados');
		}

		if($ChamadosModel->delete($id)){
			$builder = $this->db->table('chamados_historico');
			$builder->where('id_chamado', $id)->delete();

			$this->session->setFlashdata('sucesso', 'Chamado excluído com sucesso.');
		}else{
			$this->session->setFlashdata('erro', 'Não foi possível excluir o chamado.');
		}

		return redirect()->to('/chamados');

	}

}
