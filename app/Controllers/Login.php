<?php

namespace App\Controllers;
use App\Models\UserModel;

class Login extends BaseController
{
	public function index()
	{
		helper(['form']);
		return view('admin/login/login');
	}

	public function login()
	{
		helper(['form']);
		$input = $this->validate([
			'email' => [
				'label' => 'Email', 'rules' => 'required|valid_email',
				'errors' => [
					'required' => 'Preencha este campo!',
					'valid_email' => 'O e-mail deve ser válido!'
				]
			],
			'password' => [
				'label' => 'Senha', 'rules' => 'required|min_length[8]',
				'errors' => [
					'required' => 'Preencha este campo!',
					'min_length' => 'No mínimo 8 digitos!'
				]
			]
		]);

		if (!$input) {
			echo view('admin/login/login', ['validation' => $this->validator]);
		} else {
			$UserModel = new UserModel();

			$user = $UserModel->where('email', $this->request->getPost('email'))->find();

			if (count($user) == 0 or count($user) > 1) {
				$this->session->setFlashdata('erro', 'E-mail ou senha incorreta!');
				return redirect()->to('admin/login/login');
			}

			if (password_verify($this->request->getPost('password'), $user[0]->password)) {

				$session = [
					'id' => $user[0]->id,
					'name' => $user[0]->name,
					'email' => $user[0]->email,
					'foto' => $user[0]->foto,
					'perfil' => $user[0]->perfil,
					'logged_in' => true
				];

				$this->session->set($session);
				return redirect()->to('dashboard');
			} else {
				$this->session->setFlashdata('erro', 'Email ou Senha incorreto!');
				return redirect()->to('admin/login/login');
			}
		}
	}

	public function logout()
	{
		$this->session->destroy();
		return redirect()->to('/login');
	}
}
