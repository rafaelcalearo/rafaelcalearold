<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['id', 'name', 'email', 'password', 'perfil', 'foto', 'ativo', 'created_at', 'updated_at'];

    public function getUsers()
    {
        return $this->findAll();
    }
}
