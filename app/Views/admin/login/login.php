<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico" />

	<link href="<?php echo base_url('vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?php echo base_url('css/sb-admin-2.css') ?>" rel="stylesheet">
</head>

<body class="bg-gradient-primary">
	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-md-9">
				<?php my_custom_erros()  ?>
				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
							<div class="col-lg-6">
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4"><strong>BEM-VINDO!</strong></h1>
									</div>
									<?php $validation = \Config\Services::validation(); ?>
									<form action="/login" method="POST" class="user">
										<input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
										<div class="form-group">
											<input type="email" value="<?php echo set_value('email'); ?>" class="<?php if ($validation->getError('email')) {
																			echo 'form-control form-control-user is-invalid';
																		} else {
																			echo 'form-control form-control-user';
																		} ?>" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Seu e-mail" name="email">
											<?php if ($validation->getError('email')) { ?>
												<span class='small text-danger ml-3'>
													<?php echo $validation->getError('email'); ?>
												</span>
											<?php } ?>
										</div>
										<div class="form-group">
											<input type="password" value="<?php echo set_value('password'); ?>" class="<?php if ($validation->getError('password')) {
																				echo 'form-control form-control-user is-invalid';
																			} else {
																				echo 'form-control form-control-user';
																			} ?>" id="exampleInputPassword" placeholder="Senha" name="password">
											<?php if ($validation->getError('password')) { ?>
												<span class='small text-danger ml-3'>
													<?php echo $validation->getError('password'); ?>
												</span>
											<?php } ?>
										</div>
										<div class="form-group">
											<div class="custom-control custom-checkbox small">
												<input type="checkbox" class="custom-control-input" id="customCheck">
												<label class="custom-control-label" for="customCheck">Lembrar de mim</label>
											</div>
										</div>
										<button class="btn btn-primary btn-user btn-block">
											FAZER LOGIN
										</button>
										<hr>
										<a href="index.html" class="btn btn-google btn-user btn-block">
											<i class="fab fa-google fa-fw"></i> COM O GOOGLE
										</a>
										<a href="index.html" class="btn btn-facebook btn-user btn-block">
											<i class="fab fa-facebook-f fa-fw"></i> COM O FACEBOOK
										</a>
									</form>
									<hr>
									<div class="text-center">
										<a class="small" href="<?php echo base_url('#') ?>">Esqueceu a senha?</a>
									</div>
									<div class="text-center">
										Ainda sem conta? <a class="small" href="<?php echo base_url('register') ?>">Cadastre-se!</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
	<!-- <div class="text-center small mb-2">© Copyright 2021 Rafael Calearo. Alguns direitos reservados!</div>-->
	<!-- Bootstrap core JavaScript-->
	<script src="<?php echo base_url('vendor/jquery/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?php echo base_url('js/sb-admin-2.min.js') ?>"></script>
</body>

</html>